/***************************************************************************
 *   Copyright (C) 2015 Marco Martin <mart@kde.org>                        *
 *   Copyright (C) 2021 Rui Wang <wangrui@jingos.com>
 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef PHONEPANEL_H
#define PHONEPANEL_H

#include <QFutureWatcher>
#include <Plasma/Containment>

#include <KSharedConfig>
#include <KConfigWatcher>
#include <gst/gst.h>
#include "kscreeninterface.h"
#include "screenshotinterface.h"


class PhonePanel : public Plasma::Containment
{
    Q_OBJECT
    Q_PROPERTY(bool autoRotateEnabled READ autoRotate WRITE setAutoRotate NOTIFY autoRotateChanged);
    Q_PROPERTY(bool isSystem24HourFormat READ isSystem24HourFormat NOTIFY isSystem24HourFormatChanged);
    Q_PROPERTY(bool udiskInserted READ udiskInserted NOTIFY udiskInsertChanged);
    Q_PROPERTY(bool alarmVisible READ alarmVisible NOTIFY alarmStatusChanged);
public:
    PhonePanel( QObject *parent, const QVariantList &args );
    ~PhonePanel() override;

public Q_SLOTS:
    void executeCommand(const QString &command);
    void toggleTorch();
    void takeScreenshot();

    bool autoRotate();
    void setAutoRotate(bool value);
    
    bool isSystem24HourFormat();

    bool udiskInserted();

    void kcmClockUpdated();
    void handleFinished();
    bool alarmVisible();
    void alarmVisibleChanged(bool);

    void slotDeviceAdded(QString);
    void slotDeviceRemoved(QString);

signals:
    void autoRotateChanged(bool value);
    void isSystem24HourFormatChanged();
    void udiskInsertChanged(bool);
    void alarmStatusChanged(bool);

private:
    GstElement* m_pipeline;
    GstElement* m_sink;
    GstElement* m_source;
    bool m_running = false;
    bool m_udiskInsert = false;
    bool m_alarmVisible = false;
    
    KConfigWatcher::Ptr m_localeConfigWatcher;
    KSharedConfig::Ptr m_localeConfig;

    org::kde::KScreen *m_kscreenInterface;
    org::kde::kwin::Screenshot *m_screenshotInterface;

    QFutureWatcher<void> futureWatcher;
    QFuture<void> m_future;

    QString m_strScreenShot;
    bool m_initWatcherFlag;

};

#endif
